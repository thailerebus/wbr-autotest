package webet.basetest

import utils.Utils
import config.annotations.Browser
import config.annotations.Screenshot
import config.driver.Breakpoint
import config.driver.DriverFactory
import config.extensions.AttachmentHelper
import config.extensions.TestStatusLogger
import io.qameta.allure.Attachment
import mu.KotlinLogging
import org.fluentlenium.adapter.junit.jupiter.FluentTest
import org.fluentlenium.configuration.ConfigurationProperties
import org.fluentlenium.core.annotation.Page
import org.fluentlenium.core.domain.FluentWebElement
import org.fluentlenium.core.search.SearchFilter
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.openqa.selenium.By
import org.openqa.selenium.Capabilities
import org.openqa.selenium.Dimension
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeOptions
import java.time.temporal.ChronoUnit.SECONDS
import org.openqa.selenium.support.events.EventFiringWebDriver
import webet.models.Login
import webet.models.LoginModel
import webet.pageobjects.player.PlayerHomePage
import utils.YmlFileReader
import webet.pageobjects.boss.LegacyHomePage
import webet.pageobjects.boss.LegacyLoginPage
import java.nio.file.Files
import java.nio.file.Paths
import java.time.Duration
import java.util.concurrent.TimeUnit
import kotlin.math.log

@ExtendWith(TestStatusLogger::class, AttachmentHelper::class)
open class BrowserTest : FluentTest() {



	val playerBaseUrl = getPlayerBaseURL()

	val bossBaseUrl = getBossBaseURL()

	private val logger = KotlinLogging.logger {}

	private val timeout = Integer.getInteger("page_load_timeout", 100).toLong()

	var driverFactory = DriverFactory()


	override fun newWebDriver(): WebDriver {
		val chromeDirName = javaClass.canonicalName
		driverFactory.chromeDirName = chromeDirName
//		driverFactory.customCap.addArguments("user-data-dir=C:\\Users\\th.le\\chromeprofile\\" + testClassName())
		val currentDriver = driverFactory.get(requestedDriver())

		currentDriver.manage().timeouts().pageLoadTimeout(timeout, TimeUnit.SECONDS)
//		currentDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS)
		currentDriver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS)
        currentDriver.manage().window().maximize()

		return EventFiringWebDriver(currentDriver)
	}


	@BeforeEach
	fun setUp() {
		screenshotMode = screenshotMode()
		screenshotPath = "build/screenshots"
		awaitAtMost = 30_000

		events().beforeClickOn { element, _ ->
			highlight(element)
		}

		events().beforeNavigateTo { url, _ ->
			logger.info { "open URL $url" }
		}

		events().afterNavigateTo { url, _ ->
			logger.info { "opened URL $url" }
			logger.info { "window title: ${window().title()}" }
		}


//		driver.manage().deleteAllCookies()
	}


	private fun requestedDriver()= javaClass.getAnnotation(Browser::class.java)?.use

	private fun screenshotMode(): ConfigurationProperties.TriggerMode {
		if (javaClass.getAnnotation(Screenshot::class.java) != null) {
			return ConfigurationProperties.TriggerMode.MANUAL
		}
		return ConfigurationProperties.TriggerMode.AUTOMATIC_ON_FAIL
	}


	@BeforeEach
	fun changeBrowserDimension() {
		javaClass.getAnnotation(Browser::class.java)?.let {
			if (it.dimension != Breakpoint.DEFAULT) {
				driver.manageWindowSize(it.dimension)
			}
		}

	}

	private fun WebDriver.manageWindowSize(dimension: Breakpoint) {
		when (dimension) {
			Breakpoint.SMALL -> windowResizeTo(359)
			Breakpoint.MEDIUM -> windowResizeTo(599)
			Breakpoint.LARGE -> windowResizeTo(959)
			Breakpoint.XLARGE -> windowResizeTo(1199)
			Breakpoint.XXLARGE -> windowResizeTo(1280)
			Breakpoint.FULLSCREEN -> manage().window().fullscreen()
			else -> manage().window().maximize()
		}
	}

	fun WebDriver.windowResizeTo(width: Int, height: Int = 800) {
		manage().window().size = Dimension(width, height)
	}

	@AfterEach
	fun tearDown() {
		// no op

	}

	@Attachment("Attachment")
	fun getAttachmentFailedTest(filePath: String): ByteArray {
		val path = Paths.get(screenshotPath, filePath)
		return Files.readAllBytes(path)
	}

	fun jq(selector: String, vararg filter: SearchFilter) = `$`(selector, *filter)

	fun getPlayerBaseURL(): String =  Utils().getProp("player.baseURl") ?: throw RuntimeException("Empty Player base URL")
	fun getBossBaseURL(): String =  Utils().getProp("boss.baseURl") ?: throw RuntimeException("Empty boss base URL")

	@Page
	lateinit var playerHomePage : PlayerHomePage

	@Page
	lateinit var bossLegacyHomePage: LegacyHomePage

	@Page
	lateinit var bossLegacyLoginPage: LegacyLoginPage

	fun loginPlayer(): PlayerHomePage {
		val login = getLoginInfo()

		goTo(playerBaseUrl + playerHomePage.getUrl())

		await().atMost(Duration.of(10, SECONDS)).until(playerHomePage.username).present();

		playerHomePage.username.fill().with(login.username)
		playerHomePage.password.fill().with(login.password)
		playerHomePage.loginbutton.click()

		val xpath = "//a[text()='" + login.username + "']"
		await().atMost(Duration.of(20, SECONDS)).until(el(By.xpath(xpath)))
			.present()
		return playerHomePage
	}

	fun loginBossLegacy(): LegacyHomePage {

		val loginUrl = getBossBaseURL() + bossLegacyLoginPage.getUrl()
		val homeUrl = getBossBaseURL() + bossLegacyHomePage.getUrl()

		goTo(loginUrl);
		val currentUrl = driver.currentUrl

		if (!currentUrl.equals(homeUrl)) {
			bossLegacyHomePage = bossLegacyLoginPage.loginToBoss("rainbow", "123456")
		}

		return bossLegacyHomePage
	}

	private fun getLoginInfo() : Login {
		var testCurrency = System.getProperty("testCurrency") ?: "cny"
		if (testCurrency.equals("") || testCurrency.equals(null)) { testCurrency = "cny" }

		val loginModel = YmlFileReader().readResourceAsModel("/login.yml", LoginModel::class.java)
		val logins = Utils().readInstanceProperty<List<Login>>(loginModel, testCurrency)
		val loginPlayer = logins.get(0)

		return loginPlayer
	}

	fun highlight(element: FluentWebElement) {
		executeScript("arguments[0].style.border = '3px solid red'; ", element.element)
	}

}
