package webet.basetest;

import org.junit.jupiter.api.BeforeEach;
import webet.pageobjects.boss.LegacyHomePage;
import webet.pageobjects.boss.NewDepositPage;

import java.util.UUID;

public class BossTest extends BrowserTest{
    public LegacyHomePage legacyHomePage;

    public NewDepositPage newDepositPage;
    public String depositToken = UUID.randomUUID().toString();
    public String depositCode;

    public String getDepositCode() {
        return depositCode;
    }

    public void setDepositCode(String depositCode) {
        this.depositCode = depositCode;
    }

    @BeforeEach
    public void login() {
        this.legacyHomePage = loginBossLegacy();
    }

    public NewDepositPage createDeposits() {
        String depositCode;
        this.newDepositPage = this.legacyHomePage.clickOnPaymentTab()
                .goToNewDepositPage()
                .enterPlayerUsername("cny3stg")
                .selectPayementOption()
                .enterDepositAmount("100")
                .enterNote(depositToken)
                .clickSubmit();

        depositCode = newDepositPage.goToDepositList().getDepositCodeByNote(depositToken);
        this.setDepositCode(depositCode);
        return newDepositPage;
    }
}
