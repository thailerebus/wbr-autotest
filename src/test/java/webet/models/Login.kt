package webet.models

data class LoginModel(
    val cny: List<Login>,
    val idr: List<Login>
)

data class Login(
    val username: String?,
    val password: String?
)