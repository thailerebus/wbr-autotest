package webet.models.boss.DepositList;

import java.lang.reflect.Field;

public class SearchDepositByFilters_test01 {
    public String depositStatus;
    public String transactionPeriod;
    public String username;

    public String getTransactionPeriod() {
        return transactionPeriod;
    }

    public void setTransactionPeriod(String transactionPeriod) {
        this.transactionPeriod = transactionPeriod;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(String depositStatus) {
        this.depositStatus = depositStatus;

    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(": ");
        for (Field f : getClass().getFields()) {
            sb.append(f.getName());
            sb.append("=");
            try {
                sb.append(f.get(this));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            sb.append(", ");
        }
        return sb.toString();
    }
}

