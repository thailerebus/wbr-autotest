package webet.pageobjects

import io.qameta.allure.Attachment
import org.fluentlenium.core.FluentPage
import org.fluentlenium.core.domain.FluentWebElement
import java.nio.file.Files
import java.nio.file.Paths


open class BasePage : FluentPage() {

    fun highlight(element: FluentWebElement) {
        executeScript("arguments[0].style.border = '3px solid red'; ", element.element)

    }

    @Attachment
    fun attach(filePath: String): ByteArray {
        val path = Paths.get(filePath)
        return Files.readAllBytes(path)
    }

    fun saveScreenshotIgnoreElements(element: FluentWebElement) {

    }
}