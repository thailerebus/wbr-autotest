package webet.pageobjects.player

import io.qameta.allure.Step
import org.fluentlenium.core.annotation.Page
import org.fluentlenium.core.annotation.PageUrl
import org.fluentlenium.core.domain.FluentWebElement
import org.openqa.selenium.support.FindBy
import webet.pageobjects.BasePage
import java.time.Duration

@PageUrl("/en/player-center/account/profile/") // specifies getUrl()
//@FindBy(css=".enterprise-prompt") // specifies isAt()
open class UserProfilePagePlayer : BasePage() {

    @Page
    lateinit var transferPagePlayer: TransferPagePlayer

    @FindBy(xpath = "//span[contains(text(),'Transfer')]")
    lateinit var transferTab: FluentWebElement

    @Step("Click on Transfer Tab")
    fun clickOnTransferTab(): TransferPagePlayer {
        transferTab.waitAndClick(Duration.ofSeconds(5))
        return transferPagePlayer
    }
}