package webet.pageobjects.player

import io.qameta.allure.Step
import org.fluentlenium.core.annotation.Page
import org.fluentlenium.core.annotation.PageUrl
import org.fluentlenium.core.domain.FluentList
import org.fluentlenium.core.domain.FluentWebElement
import org.fluentlenium.core.filter.FilterConstructor.withText
import org.openqa.selenium.By
import org.openqa.selenium.support.FindBy
import webet.pageobjects.BasePage
import java.lang.Thread.sleep
import java.time.Duration
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals

@PageUrl("/en/player-center/Transfer") // specifies getUrl()
//@FindBy(css=".enterprise-prompt") // specifies isAt()
open class TransferPagePlayer : BasePage() {

    @Page
    lateinit var TransferPagePlayer: TransferPagePlayer

    var beforeToWalletAmount: String = ""
    var beforeFromWalletAmount: String = ""
    var currentToWalletAmount: String = ""
    var currentFromWalletAmount: String = ""
    var amountTransfer: String = ""

    lateinit var subWalletElement: FluentWebElement


    @FindBy(xpath = "//*/select")
    lateinit var listOfSelectElement: FluentList<FluentWebElement>

    @FindBy(css = "input[type='tel']")
    lateinit var transferAmountEl: FluentWebElement


    @Step("Select \'Transfer From\' and \'Transfer To\' wallet")
    fun selectFromAndToWallet(fromWallet: String, toWallet: String): TransferPagePlayer {
        beforeToWalletAmount = getCurrentAmount(toWallet)
        beforeFromWalletAmount = getCurrentAmount(fromWallet)

        listOfSelectElement[0].click().el(withText().contains(fromWallet)).click()
        listOfSelectElement[1].click().el(withText().contains(toWallet)).click()

        return this
    }

    @Step("Enter amount to transfer")
    fun enterAmountTransfer(amount: String): TransferPagePlayer {
        amountTransfer = amount
        transferAmountEl.fill().with(amount)
        sleep(1000)
        return this
    }

    @Step("Click Transfer button and close the message box")
    fun clickTransferButtonAndConfirm(): TransferPagePlayer {
        el(By.xpath("//*/button/span[contains(text(),'Transfer')]")).click()
//        val message = el(By.cssSelector()).find(By.tagName("p")).first()

        await().atMost(10, TimeUnit.SECONDS).until(el(By.cssSelector(".notification-message-box")).find(By.tagName("p")).first())
            .text("Transferring of money has been successfully made!")
        el(By.xpath("//a[contains(text(),'close')]")).click()
        return this

    }

    private fun getCurrentAmount(walletName: String): String {
        var xpath: String =  "//div[contains(text(), '" + walletName + "')]/parent::div/following-sibling::div/div/span[2]"
        val amountEl =  el(By.xpath(xpath))
        await().atMost(Duration.ofSeconds(5)).until(amountEl).present()

        var currentAmount = amountEl.text()
        highlight(amountEl)

        return currentAmount
    }

    @Step("Verify amount of From Wallet after transfer")
    fun verifyFromWalletAfterTransfer(fromWallet: String): TransferPagePlayer {
        var expectFromWalletAmount: Float = beforeFromWalletAmount.toFloat() - amountTransfer.toFloat()
        var currentFromWalletAmount: Float = getCurrentAmount(fromWallet).toFloat()

        assertEquals(expectFromWalletAmount, currentFromWalletAmount, "Expect after transfer amount of " + fromWallet  + " is " + expectFromWalletAmount + " but current is: " + currentFromWalletAmount)
        return TransferPagePlayer
    }

    @Step("Verify amount of To Wallet after transfer")
    fun verifyToWalletAfterTransfer(toWallet: String): TransferPagePlayer {
        var expectToWalletAmount: Float = beforeToWalletAmount.toFloat() + amountTransfer.toFloat() + 1 // + 1 to fail the test
        var currentToWalletAmount: Float = getCurrentAmount(toWallet).toFloat()

        assertEquals(expectToWalletAmount, currentToWalletAmount, "Expect after transfer amount of " + toWallet  + " is " + expectToWalletAmount + " but current is: " + currentToWalletAmount)
        return TransferPagePlayer
    }



}
