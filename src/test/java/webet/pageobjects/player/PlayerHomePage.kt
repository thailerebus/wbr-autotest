package webet.pageobjects.player

import io.qameta.allure.Step
import org.fluentlenium.core.annotation.Page
import org.fluentlenium.core.annotation.PageUrl
import org.fluentlenium.core.domain.FluentWebElement
import org.openqa.selenium.By
import org.openqa.selenium.support.FindBy
import webet.pageobjects.BasePage

@PageUrl( "/en") // specifies getUrl()
//@FindBy(css=".enterprise-prompt") // specifies isAt()
open class PlayerHomePage : BasePage() {

    @Page
    lateinit var userProfilePagePlayer: UserProfilePagePlayer

    @FindBy(css = "input[placeholder=\"Username\"]")
    lateinit var username: FluentWebElement

    @FindBy(css = "input[placeholder=\"Password\"]")
    lateinit var password: FluentWebElement

    @FindBy(xpath = "//span[contains(text(),'Login')]")
    lateinit var loginbutton: FluentWebElement

    @Step("Click on user name")
    fun clickOnUserProfile(): UserProfilePagePlayer {
        el(By.cssSelector("a[href=\"/en/player-center/account/profile/\"]")).click()
        return userProfilePagePlayer
    }

}