package webet.pageobjects.player

import io.qameta.allure.Attachment
import org.fluentlenium.core.FluentPage
import java.nio.file.Files
import java.nio.file.Paths

open class GamePage: FluentPage() {

    @Attachment("Attachment")
    fun getAttachment(filePath: String): ByteArray {
        val path = Paths.get(filePath)
        return Files.readAllBytes(path)
    }

}