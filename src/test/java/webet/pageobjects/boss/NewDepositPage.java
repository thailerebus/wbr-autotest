package webet.pageobjects.boss;

import com.google.common.base.Function;
import io.qameta.allure.Step;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.sikuli.script.Key;
import webet.features.boss.DepositList.SearchDepositByFilters;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.fluentlenium.core.filter.FilterConstructor.with;

@PageUrl("/payment-management/deposit-list")
public class NewDepositPage extends FluentPage {

    @Page
    DepositListPage depositListPage;

    @FindBy(css = "[autocomplete='off']")
    FluentWebElement username;

    @Step("Enter user name then tab")
    public NewDepositPage enterPlayerUsername(String username) {
        await().atMost(Duration.of(2, SECONDS)).until(this.username).present();
        this.username.fill().with(username);

        this.username.getElement().sendKeys(Key.TAB);

        await().atMost(Duration.of(10, SECONDS))
                .pollingEvery(Duration.of(100, MILLIS))
                .until($(".loading")).size(0);
        return this;
    }

    @Step("Select Payment Option")
    public NewDepositPage selectPayementOption() {


        FluentWebElement selectPayment =  el(By.cssSelector("[placeholder=\"Select Payment Option\"]"));
        await().explicitlyFor(3000);
        selectPayment.click();

        FluentWebElement payment = el(By.xpath("//div[contains(text(),'UnionPay QR Code')]"));

        Boolean paymentDisplayed = await().atMost(5, TimeUnit.SECONDS)
                .until(payment)
                .displayed();

        if (!paymentDisplayed) {
            selectPayment.click();
        }

        payment.click();
        return this;
    }

    @Step("Enter deposit amount")
    public NewDepositPage enterDepositAmount(String amount) {
        el(By.cssSelector("[name=\"depositAmount\"]")).click().fill().withText(amount);
        return this;
    }
    @Step("Enter deposit note")
    public NewDepositPage enterNote(String note) {
        el(By.cssSelector("[name=\"note\"]")).fill().withText(note);
        return this;
    }

    @Step("Click submit")
    public NewDepositPage clickSubmit() {
        el(By.xpath("//button[text()='Submit']")).waitAndClick();

        waitForSuccessMessageDisplayed("successful");
        return this;
    }

    public void waitForSuccessMessageDisplayed(String keyword) {
        String xpath = "//*[contains(text(),'" + keyword + "')]";
        await().atMost(Duration.of(10, SECONDS))
                .pollingEvery(Duration.of(50, MILLIS))
                .until(() -> el(By.xpath(xpath)).present());
    }

    public DepositListPage goToDepositList() {
        el(By.cssSelector("a[href='/payment-management/deposit-list']")).click();
        return depositListPage;
    }
}
