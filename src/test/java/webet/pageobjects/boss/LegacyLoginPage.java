package webet.pageobjects.boss;

import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

@PageUrl("/auth/login")
public class LegacyLoginPage extends FluentPage {
    @Page
    LegacyHomePage bossLegacyHomePage;

    @FindBy(css = "[name=\"login\"]")
    FluentWebElement login;

    @FindBy(css = "[name=\"password\"]")
    FluentWebElement password;

    @FindBy(css = "[name=\"submit\"]")
    FluentWebElement submitButton;

    public LegacyHomePage loginToBoss(String username, String password) {
        this.login.fill().withText(username);
        this.password.fill().withText(password);
        this.submitButton.click();
        return bossLegacyHomePage;
    }
}
