package webet.pageobjects.boss;

import io.qameta.allure.Step;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.assertj.core.api.Assertions.*;
import static org.fluentlenium.core.filter.FilterConstructor.with;

@PageUrl("/payment-management/deposit-list")
public class DepositListPage extends FluentPage {

    @Page
    NewDepositPage newDepositPage;

    @Step("Enter Deposit Status")
    public DepositListPage chooseDepositStatus(String depositStatus) throws InterruptedException {

        waitLoadingBoxToDisappear();
        String xpath = "//label[text()='" + depositStatus + "']";

        FluentWebElement boxToClick =  el(By.cssSelector("[name='depositStatus']")).find(By.xpath(xpath)).first();
        FluentWebElement pendingBox =  el(By.cssSelector("[name='depositStatus']")).find(By.xpath("//label[text()='Pending']")).first();

        pendingBox.click();
        boxToClick.click();


        return this;
    }

    @Step("Enter deposit code")
    public DepositListPage enterDepositCode(String depositCode) {
        waitLoadingBoxToDisappear();

        FluentWebElement depositCodeField = el(By.cssSelector("[name=\"depositCode\"]"));

        depositCodeField.fill().withText(depositCode);
        return this;
    }

    @Step("Click search and scroll to Deposit list")
    public DepositListPage clickSearchScrollToRequestList() {

        FluentWebElement searchBtn = el(By.xpath("//button[contains(text(),'Search')]"));

        awaitElementToDisplay(5, searchBtn);
        searchBtn.click();

        FluentWebElement h3 =  el(By.xpath("//h3[contains(text(),'Deposit Request List')]"));

        executeScript("arguments[0].scrollIntoView(true);", h3.getElement());
        return this;
    }

    @Step("Verify Deposit is found")
    public DepositListPage verifyDepositIsFound(String depositCode) {

        String xpath = "//tr[td//text()[contains(., '" + depositCode + "')]]";

        Boolean existed =  el(By.xpath(xpath)).present();

        assertThat(existed).isTrue();
        return this;
    }

    @Step("Verify Deposit with status {0} is found")
    public DepositListPage verifyDepositWithStatusIsFound(String depositStatus) {
        FluentWebElement table = el(By.cssSelector(".table"));

        FluentList rows = table.find(By.tagName("tr"));

        assertThat(rows.size()).isGreaterThan(0);
        return this;
    }


    public NewDepositPage goToNewDepositPage() {
        FluentWebElement el = el(By.cssSelector("[href=\"/payment-management/new-deposit\"]"));
        await().atMost(Duration.of(20,SECONDS))
                .until(el).present();
        el.click();
        return newDepositPage;
    }

    public String getDepositCodeByNote(String note) {
        waitLoadingBoxToDisappear();

        FluentWebElement h3 =  el(By.xpath("//h3[contains(text(),'Deposit Request List')]"));
        executeScript("arguments[0].scrollIntoView(true);", h3.getElement());
        String xpath = "//tr[td//text()[contains(., '" + note + "')]]";

        FluentWebElement searchRow = el(By.xpath(xpath));
        FluentWebElement depositCode = searchRow.find(By.tagName("td")).get(1);

        return  depositCode.text();
    }

    private void waitLoadingBoxToDisappear() {
        try {
            Boolean loading = await().atMost(Duration.of(4,SECONDS))
                    .pollingEvery(Duration.of(49, MILLIS))
                    .until($(".loading"))
                    .present();
            if (loading) {
                await().atMost(10, TimeUnit.SECONDS)
                        .until($(".loading")).size(0);
            }
        }
        catch (Exception e) {
            System.out.println(e.getCause());
        }

    }

    private void awaitElementToDisplay(int timeoutSECONDS, FluentWebElement el) {
        await().atMost(Duration.of(timeoutSECONDS, SECONDS))
                .until(el).displayed();
    }

}
