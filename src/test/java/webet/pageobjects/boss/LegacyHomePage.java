package webet.pageobjects.boss;

import io.qameta.allure.Step;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

@PageUrl("/home")
public class LegacyHomePage extends FluentPage {

    @Page
    DepositListPage bossDepositListPage;

    @FindBy(css = "[href=\"/go_to/new_boss?page=payment-management/deposit-list\"]")
    FluentWebElement paymentTab;

    @Step("Go to deposit list")
    public DepositListPage clickOnPaymentTab()  {
        paymentTab.click();
        return bossDepositListPage;
    }
}
