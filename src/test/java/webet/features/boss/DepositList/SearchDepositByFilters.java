package webet.features.boss.DepositList;

import config.extensions.TestModelProvider;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import webet.basetest.BossTest;
import webet.basetest.BrowserTest;
import webet.models.boss.DepositList.*;
import webet.pageobjects.boss.LegacyHomePage;
import webet.pageobjects.boss.LegacyLoginPage;
import webet.pageobjects.boss.NewDepositPage;

import java.util.UUID;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Feature("Boss Deposit List")
public class SearchDepositByFilters extends BossTest {

    @ParameterizedTest
    @ArgumentsSource(TestModelProvider.class)
    @Story("Search deposit requests by Deposit status")
    public void test01(SearchDepositByFilters_test01 testData) throws InterruptedException {

        legacyHomePage.clickOnPaymentTab()
                .chooseDepositStatus(testData.getDepositStatus())
                .clickSearchScrollToRequestList()
                .verifyDepositWithStatusIsFound(testData.getDepositStatus());
    }

    @Test
    @Story("Search Deposits by Deposit code")
    public void test02() {
        createDeposits();
        newDepositPage.goToDepositList()
                .enterDepositCode(getDepositCode())
                .clickSearchScrollToRequestList()
                .verifyDepositIsFound(getDepositCode());

    }

}
