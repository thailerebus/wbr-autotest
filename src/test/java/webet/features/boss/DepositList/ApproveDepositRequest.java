package webet.features.boss.DepositList;

import config.extensions.TestModelProvider;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.aspectj.lang.annotation.Before;
import org.fluentlenium.core.annotation.Page;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import webet.basetest.BossTest;
import webet.basetest.BrowserTest;
import webet.models.boss.DepositList.ApproveDepositRequest_test01;
import webet.models.boss.DepositList.SearchDepositByFilters_test01;
import webet.pageobjects.boss.LegacyHomePage;
import webet.pageobjects.boss.NewDepositPage;

import java.lang.reflect.Array;
import java.util.UUID;

@Feature("Boss Deposit List")
public class ApproveDepositRequest extends BossTest {

    private String[] depositCodes = new String[2];
    @Tag("Regression")
    @Test
    @Story("Search deposit by deposit code then approve")
    public void test01() throws InterruptedException {

        createDeposits();
        newDepositPage.goToDepositList()
                .enterDepositCode(getDepositCode())
                .clickSearchScrollToRequestList()
                .verifyDepositIsFound(getDepositCode());
    }

}

