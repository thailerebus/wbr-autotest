package webet.features.player.Transfer

import webet.basetest.BrowserTest
import config.annotations.Browser
import config.driver.Breakpoint
import config.driver.Browsers
import config.extensions.TestModelProvider
import io.qameta.allure.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ArgumentsSource
import webet.models.player.Transfer.TransferFlow1_test_1

@Browser(dimension = Breakpoint.MAXIMIZED, use = Browsers.CHROME)
@Feature("Regression Test")
open class TransferFlow1 : BrowserTest() {
    @ParameterizedTest
    @ArgumentsSource(TestModelProvider::class)
    @Description("User log on and make his second transfer to multiple sub-wallet")
    fun test_1(testdata: TransferFlow1_test_1) {

        val playerHomePage = loginPlayer()
        playerHomePage.clickOnUserProfile()
            .clickOnTransferTab()
            .selectFromAndToWallet(testdata.fromWallet, testdata.toWallet)
            .enterAmountTransfer(testdata.amount)
            .clickTransferButtonAndConfirm()
            .verifyFromWalletAfterTransfer(testdata.toWallet)
            .verifyToWalletAfterTransfer(testdata.fromWallet)
    }

/*
* Sample parameterized test
*
* */
  /*  @ParameterizedTest
    @ArgumentsSource(TestModelProvider::class)
    fun test(testdata: TransferFlow1_test_1){
       assertEquals("1", testdata.amount)
    }
*/

}