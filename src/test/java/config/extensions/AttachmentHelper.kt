package config.extensions

import org.fluentlenium.configuration.ConfigurationProperties
import webet.basetest.BrowserTest
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.TestWatcher
import java.util.*

class AttachmentHelper : TestWatcher {

    override fun testSuccessful(context: ExtensionContext) {}

    override fun testDisabled(context: ExtensionContext, reason: Optional<String>) {}

    override fun testAborted(context: ExtensionContext, cause: Throwable) {}

    override fun testFailed(context: ExtensionContext, cause: Throwable) {
        var testinstance = context.testInstance. get() as BrowserTest
        if (testinstance.screenshotMode == ConfigurationProperties.TriggerMode.AUTOMATIC_ON_FAIL) {
            val fileName = context.testClass.get().simpleName + "_" + context.testMethod.get().name + ".png"
            testinstance.getAttachmentFailedTest(fileName)
        }

    }
}