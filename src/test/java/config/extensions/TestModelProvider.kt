package config.extensions

import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import utils.YmlFileReader
import java.util.stream.Stream

class TestModelProvider : ArgumentsProvider{

    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> {
        val testName = context?.testClass?.get()?.canonicalName
        val testMethod = context?.testMethod?.get()?.name
        val modelName =  testName?.replace("features", "models") + "_" + testMethod

        val modelClassType = Class.forName(modelName)
        print(testName)
        val testModels = YmlFileReader().readResourceYmlAsArrayOfModels(modelClassType)
        var arguments = arrayOf<Arguments>()
        for (arg in testModels)
            arguments += Arguments.of(arg)

        return Stream.of(*arguments)
    }

}