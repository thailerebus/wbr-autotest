package utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.junit.jupiter.api.Test as test
import java.nio.file.Paths

class YmlFileReader {

    fun <T> getFileName(clazz: Class<T>) :String {
        val modelPath = "/model"
        var fileName = clazz.simpleName + ".yml"

        return Paths.get( modelPath, fileName).toString().replace("\\","/")
    }

    fun <T> readResourceAsModel(resourceFile: String, model: Class<T>): T{
        YmlFileReader::class.java.getResourceAsStream(resourceFile).use {
            val mapper = ObjectMapper(YAMLFactory()) // Enable YAML parsing
            mapper.registerModule(KotlinModule())
            return mapper.readValue(it, mapper.typeFactory.constructType(model))
        }
    }

    fun <T> readResourceYmlAsArrayOfModels(clazz: Class<T>): Array<T> {
        val filePath = getFileName(clazz)
        System.out.println(filePath)
        YmlFileReader::class.java.getResourceAsStream(filePath).use {
            val mapper = ObjectMapper(YAMLFactory()) // Enable YAML parsing
            mapper.registerModule(KotlinModule())
            return mapper.readValue(it, mapper.typeFactory.constructArrayType(clazz))
        }
    }

}